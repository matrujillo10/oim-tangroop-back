const { Client } = require('pg')
const dotenv = require('dotenv');
dotenv.config();

class DBManager {
    constructor() {}

    //-------------------------------------------------------------------------------------------
    // Tools
    //-------------------------------------------------------------------------------------------

    getTools() {
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        client.connect()
        console.log(`SELECT * FROM public."TOOLS"()`)
        return [client.query('SELECT * FROM public."TOOLS"()'), client]
    }

    getTool(id) {
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        client.connect()
        console.log(`SELECT * FROM public."TOOL"(${id})`)
        return [client.query('SELECT * FROM public."TOOL"($1::int)', [id]), client]
    }

    getToolsByTags(data) {
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        client.connect()
        console.log(`SELECT * FROM public."TOOLS_BY_TAGS"(\'${data.population_approach}\', \'${data.functional_category}\', ARRAY[${data.tags == undefined ? [] : data.tags}])`)
        return [client.query('SELECT * FROM public."TOOLS_BY_TAGS"($1::text, $2::text, $3::int[])', 
        [data.population_approach, data.functional_category, data.tags == undefined ? [] : data.tags]), client]
    }

    getToolsRelated(id) {
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        client.connect()
        console.log(`SELECT * FROM public."TOOLS_RELATED"(${id})`)
        return [client.query('SELECT * FROM public."TOOLS_RELATED"($1::int)', [id]), client]
    }

    getTagsByTool(id) {
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        client.connect()
        console.log(`SELECT * FROM public."TAGS_BY_TOOL"(${id})`)
        return [client.query('SELECT * FROM public."TAGS_BY_TOOL"($1::int)', [id]), client]
    }

    getFilesByTool(id) {
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        client.connect()
        console.log(`SELECT * FROM "oim_toolfile" WHERE "tool_id" = ${id}`)
        return [client.query('SELECT * FROM "oim_toolfile" WHERE "tool_id" = $1::int', [id]), client]
    }

    //-------------------------------------------------------------------------------------------
    // Tags
    //-------------------------------------------------------------------------------------------

    getTagsByPattern(pattern, type) {
	    var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        var str = pattern.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
        client.connect()
        switch (type) {
            case '0':
                console.log(`SELECT functional_category FROM "oim_tool" WHERE unaccent(LOWER("functional_category")) LIKE '%${str}%'`)
                return [client.query('SELECT functional_category FROM "oim_tool" WHERE unaccent(LOWER("functional_category")) LIKE \'%\' || $1::text || \'%\'', [str]), client]
            case '1':
                console.log(`SELECT population_approach FROM "oim_tool" WHERE unaccent(LOWER("population_approach")) LIKE '%${str}%'`)
                return [client.query('SELECT population_approach FROM "oim_tool" WHERE unaccent(LOWER("population_approach")) LIKE \'%\' || $1::text || \'%\'', [str]), client]
        }
        
    }

    getTags(data) {
        console.log(process.env.DATABASE_URL);
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        console.log(data.tags)
        if (data.pattern && data.pattern != '') {
            var str = data.pattern.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
            client.connect()
            console.log(`SELECT * FROM public."TAGS_BY_TAGS"('${data.population_approach}', '${data.functional_category}', ARRAY[${data.tags == undefined ? [] : data.tags}]) WHERE unaccent(LOWER("name")) LIKE unaccent('%${str}%')`)
            return [client.query('SELECT * FROM public."TAGS_BY_TAGS"($1::text, $2::text, $3::int[]) WHERE levenshtein(unaccent(LOWER("name")), unaccent($4::text)) <= GREATEST(1, CEIL(length($4::text)*.2))', [data.population_approach, data.functional_category, data.tags == undefined ? [] : data.tags, str]), client]
        } else {
            client.connect()
            console.log(`SELECT * FROM public."TAGS_BY_TAGS"('${data.population_approach}', '${data.functional_category}', ARRAY[${data.tags == undefined ? [] : data.tags}])`)
            return [client.query('SELECT * FROM public."TAGS_BY_TAGS"($1::text, $2::text, $3::int[])', [data.population_approach, data.functional_category, data.tags == undefined ? [] : data.tags]), client]
        }
    }

    getPA(data) {
        console.log(process.env.DATABASE_URL);
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        if (data.pattern && data.pattern != '') {
            var str = data.pattern.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
            client.connect()
            console.log(`SELECT * FROM public."PA_BY_TAGS"('${data.functional_category}', ARRAY[${data.tags == undefined ? '' : []}]) WHERE unaccent(LOWER("name")) LIKE unaccent('%${str}%')`)
            return [client.query('SELECT * FROM public."PA_BY_TAGS"($1::text, $2::int[]) WHERE unaccent(LOWER("name")) LIKE unaccent(\'%\' || $3::text || \'%\')', [data.functional_category, data.tags == undefined ? [] : data.tags, str]), client]
        } else {
            client.connect()
            console.log(`SELECT * FROM public."PA_BY_TAGS"('${data.functional_category}', ARRAY[${data.tags == undefined ? [] : data.tags}])`)
            return [client.query('SELECT * FROM public."PA_BY_TAGS"($1::text, $2::int[])', [data.functional_category, data.tags == undefined ? [] : data.tags]), client]
        }
    }

    getFC(data) {
        console.log(process.env.DATABASE_URL);
        var client = new Client({
            connectionString: process.env.DATABASE_URL,
        })
        if (data.pattern && data.pattern != '') {
            var str = data.pattern.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
            client.connect()
            console.log(`SELECT * FROM public."FC_BY_TAGS"('${data.population_approach}', ARRAY[${data.tags == undefined ? [] : data.tags}]) WHERE unaccent(LOWER("name")) LIKE unaccent('%${str}%')`)
            return [client.query('SELECT * FROM public."FC_BY_TAGS"($1::text, $2::int[]) WHERE unaccent(LOWER("name")) LIKE unaccent(\'%\' || $3::text || \'%\')', [data.population_approach, data.tags == undefined ? [] : data.tags, str]), client]
        } else {
            client.connect()
            console.log(`SELECT * FROM public."FC_BY_TAGS"('${data.population_approach}', ARRAY[${data.tags == undefined ? [] : data.tags}])`)
            return [client.query('SELECT * FROM public."FC_BY_TAGS"($1::text, $2::int[])', [data.population_approach, data.tags == undefined ? [] : data.tags]), client]
        }
    }
}


module.exports = DBManager;
