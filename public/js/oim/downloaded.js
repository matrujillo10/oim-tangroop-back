(function ($) {
  "use strict"; // Start of use strict

  function openDB() {
    return new Promise(function (resolve, reject) {
      var db;
      var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
      var request = indexedDB.open('tools-db');

      request.onerror = function (event) {
        console.log('The database is opened failed');
        reject(event);
      };

      request.onsuccess = function (event) {
        db = request.result;
        resolve(db);
      };

      request.onupgradeneeded = function (event) {
        db = event.target.result;
        var objectStore;
        if (!db.objectStoreNames.contains('tools')) {
          objectStore = db.createObjectStore('tools', { keyPath: 'id' });
          objectStore.createIndex('title', 'title', { unique: false });
        }
        resolve(db);
      }
    });
  }

  function readAll(db) {
    return new Promise((resolve, reject) => {
      var objectStore = db.transaction('tools').objectStore('tools');
      var tools = []; 
      objectStore.openCursor().onsuccess = function (event) {
        var cursor = event.target.result;
        if (cursor) {
          tools.push({
            id: cursor.value.id,
            product_title: cursor.value.product_title,
            functional_category: cursor.value.functional_category,
            cover_class: cursor.value.cover_class,
            card_class: cursor.value.card_class,
          });
          cursor.continue();
        } else {
          resolve(tools);
        }
      };
    });
  }

  function addCard(e, first) {
    $('#cards-container').append(
      `<div id="${first ? 'w-node-ca8dcf39e7a5-a3137c58' : `card-tool-${e.id}`}" class="${e.card_class}">
        <div class="${e.cover_class}"></div>
        <div class="div-block-61">
          <p class="tipoherramienta">${e.functional_category}<br></p>
          <h1 class="tituloherramienta">
            ${e.product_title}
            <br>
          </h1>
          <a href="herramienta-guardada.html?toolID=${e.id}" class="vinculo">Detalle del recurso→</a></div>
      </div>`
    )
  }

  function filter(pattern, tools) {
    if (!pattern || pattern === '') {
      return tools;
    }
    var filtered = [];
    pattern = pattern.toLowerCase();
    for (const tool of tools) {
      if(tool.functional_category.toLowerCase().indexOf(pattern) > -1 
        || tool.product_title.toLowerCase().indexOf(pattern) > -1) {
        filtered.push(tool);
      }
    }
    return filtered;
  }

  openDB().then(db => {
    readAll(db).then(tools => {
      // Buscador
      $('#search-bar').on("change paste keyup", function () {
        $('#cards-container').empty();
        var k = 0;
        for (const tool of filter($(this).val(), tools)) {
          addCard(tool, k++ == 0);
        }
      });
      
      var k = 0;
      for (const tool of tools) {
        addCard(tool, k++ == 0);
      }
      
    });
  }).catch(error => {
    console.log(error);
    $('#cards-container').empty();
    $('#cards-container').append(`
      <h1 class="my-4">Algo anda mal con tu dispositivo...
        <small>Si el problema continua, intenta contactar al administrador.</small>
      </h1>
      `);
  });
})(jQuery); // End of use strict
