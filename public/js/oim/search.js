(function ($) {
  "use strict"; // Start of use stricts

  function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
    }
  }

  $("#search_bar_pa").select2({
    placeholder: 'Enfoque poblacional',
    allowClear: true,
    minimumInputLength: 0,
    cache: true,
    delay: 250,
    ajax: {
      method: "POST",
      url: '/api/tags/pa',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      data: function (params) {
        var query = {
          pattern: params.term,
          functional_category: $("#search_bar_fc").val(),
          tags: $("#search_bar_tags").val()
        }
        return JSON.stringify(query);
      },
      processResults: function (data) {
        var results = []
        data.forEach(e => {
          results.push({
            id: e.id,
            text: e.name
          })
        });
        return {
          results: results
        };
      }
    }
  });

  $("#search_bar_fc").select2({
    placeholder: 'Tipo de recurso',
    allowClear: true,
    minimumInputLength: 0,
    cache: true,
    delay: 250,
    ajax: {
      method: "POST",
      url: '/api/tags/fc',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      data: function (params) {
        var query = {
          pattern: params.term,
          population_approach: $("#search_bar_pa").val(),
          tags: $("#search_bar_tags").val()
        }
        return JSON.stringify(query);
      },
      processResults: function (data) {
        var results = []
        data.forEach(e => {
          results.push({
            id: e.id,
            text: e.name
          })
        });
        return {
          results: results
        };
      }
    }
  });

  $("#search_bar_tags").select2({
    placeholder: 'Palabras claves',
    allowClear: true,
    minimumInputLength: 0,
    cache: true,
    delay: 250,
    ajax: {
      method: "POST",
      url: '/api/tags/thesaurus',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      data: function (params) {
        var query = {
          pattern: params.term,
          population_approach: $("#search_bar_pa").val(),
          functional_category: $("#search_bar_fc").val(),
          tags: $("#search_bar_tags").val()
        }
        return JSON.stringify(query);
      },
      processResults: function (data) {
        var results = []
        data.forEach(e => {
          results.push({
            id: e.id,
            text: e.name
          })
        });
        return {
          results: results
        };
      }
    }
  });

  function selectAndUnselectEvent(data) {
    $.ajax({
      method: "POST",
      url: '/api/tools/by-tags',
      data: JSON.stringify(data),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8'
    }).done(data => {
      $(`#cards-container`).empty()
      var k = 0;
      data.forEach(e => {
        addCard(e, k++ == 0)
      })
      if (data.length == 0) {
        $(`#cards-container`).empty()
      }
      $('#count-tools').text(`Herramientas encontradas: ${data.length}`);
    })
  }

  $('#search_bar_pa').on('select2:select', function (e) {
    selectAndUnselectEvent({
      population_approach: $("#search_bar_pa").val(),
      functional_category: $("#search_bar_fc").val(),
      tags: $("#search_bar_tags").val()
    })
  })

  $('#search_bar_fc').on('select2:select', function (e) {
    selectAndUnselectEvent({
      population_approach: $("#search_bar_pa").val(),
      functional_category: $("#search_bar_fc").val(),
      tags: $("#search_bar_tags").val()
    })
  })

  $('#search_bar_tags').on('select2:select', function (e) {
    selectAndUnselectEvent({
      population_approach: $("#search_bar_pa").val(),
      functional_category: $("#search_bar_fc").val(),
      tags: $("#search_bar_tags").val()
    })
  })

  $('#search_bar_pa').on('select2:unselecting', function (e) {
    selectAndUnselectEvent({
      population_approach: '',
      functional_category: $("#search_bar_fc").val(),
      tags: $("#search_bar_tags").val()
    })
  });

  $('#search_bar_fc').on('select2:unselecting', function (e) {
    selectAndUnselectEvent({
      population_approach: $("#search_bar_pa").val(),
      functional_category: '',
      tags: $("#search_bar_tags").val()
    })
  });

  $('#search_bar_tags').on('select2:unselecting', function (e) {
    var tags = []
    $('#search_bar_tags').val().forEach(el => {
      if (e.params.args.data.id != el) {
        tags.push(el);
      }
    })
    selectAndUnselectEvent({
      population_approach: $("#search_bar_pa").val(),
      functional_category: $("#search_bar_fc").val(),
      tags: tags
    })
  })

  // Tool type pre-filter
  var toolType = getUrlParameter('toolType');
  if (toolType != null && toolType != undefined) {
    $.ajax({
      method: "GET",
      url: '/api/tags',
      data: {
        pattern: toolType,
        type: 0
      },
      dataType: 'json',
      contentType: 'application/json; charset=utf-8'
    }).done(data => {
      if (data.length > 0) {
        var newOption = new Option(data[0].functional_category, data[0].functional_category, false, true);
        $('#search_bar_fc').append(newOption).trigger('change');
        selectAndUnselectEvent({
          population_approach: $("#search_bar_pa").val(),
          functional_category: $("#search_bar_fc").val(),
          tags: $("#search_bar_tags").val()
        })
      }
    })
  }

  // Population approach pre-filter
  var populationApproach = getUrlParameter('pa');
  if (populationApproach != null && populationApproach != undefined) {
    $.ajax({
      method: "GET",
      url: '/api/tags',
      data: {
        pattern: populationApproach,
        type: 1
      },
      dataType: 'json',
      contentType: 'application/json; charset=utf-8'
    }).done(data => {
      if (data.length > 0) {
        var newOption = new Option(data[0].population_approach, data[0].population_approach, false, true);
        $('#search_bar_pa').append(newOption).trigger('change');
        selectAndUnselectEvent({
          population_approach: $("#search_bar_pa").val(),
          functional_category: $("#search_bar_fc").val(),
          tags: $("#search_bar_tags").val()
        })
      }
    })
  }


  function addCard(e, first) {
    $('#cards-container').append(
      `<div id="${first ? 'w-node-ca8dcf39e7a5-a3137c58' : `card-tool-${e.id}`}" class="${e.card_class}">
        <div class="${e.cover_class}"></div>
        <div class="div-block-61">
          <p class="tipoherramienta">${e.functional_category}<br></p>
          <h1 class="tituloherramienta">
            ${e.product_title}
            <br>
          </h1>
          <a href="resultado-herramienta.html?toolID=${e.id}" class="vinculo retrieve-search" 
              onclick="localStorage.setItem('population_approach', $('#search_bar_pa').val());
              localStorage.setItem('functional_category', $('#search_bar_fc').val());
              localStorage.setItem('tags', JSON.stringify($('#search_bar_tags').select2('data')));
              return true;">Detalle del recurso→</a>
        </div>
      </div>`
    )
  }

  var retrieve = getUrlParameter('retrieve');
  if (retrieve != null && retrieve != undefined) {
    var pa = localStorage.getItem('population_approach');
    if (pa != null && pa != undefined && pa != 'null' && pa != '') {
      var newOption = new Option(pa, pa, false, true);
      $('#search_bar_pa').append(newOption).trigger('change');
    }

    var fc = localStorage.getItem('functional_category');
    if (fc != null && fc != undefined && fc != 'null' && fc != '') {
      newOption = new Option(fc, fc, false, true);
      $('#search_bar_fc').append(newOption).trigger('change');
    }

    var tags = localStorage.getItem('tags');
    if (tags != null && tags != undefined && tags != 'null' && tags != '') {
      tags = JSON.parse(tags);
      for (let tag of tags) {
        newOption = new Option(tag.text, tag.id, false, true);
        $('#search_bar_tags').append(newOption).trigger('change');
      }
    }

    selectAndUnselectEvent({
      population_approach: $("#search_bar_pa").val(),
      functional_category: $("#search_bar_fc").val(),
      tags: $("#search_bar_tags").val()
    })
  } else {
    // El llamado viene desde otra parte, así que se borra los parametros de busqueda 
    localStorage.clear()
  }
})(jQuery); // End of use strict