(function ($) {
    "use strict"; // Start of use strict


    function openDB() {
        return new Promise(function (resolve, reject) {
            var db;
            var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
            var request = indexedDB.open('tools-db');

            request.onerror = function (event) {
                console.log('The database is opened failed');
                reject(event);
            };

            request.onsuccess = function (event) {
                db = request.result;
                resolve(db);
            };

            request.onupgradeneeded = function (event) {
                db = event.target.result;
                var objectStore;
                if (!db.objectStoreNames.contains('tools')) {
                    objectStore = db.createObjectStore('tools', { keyPath: 'id' });
                    objectStore.createIndex('id', 'id', { unique: false });
                }
                resolve(db);
            }
        });
    }

    function addCard(e, first) {
        $('#related-tools').append(
          `<div id="${first ? 'w-node-ca8dcf39e7a5-a3137c58' : `card-tool-${e.id}`}" class="${e.card_class}">
            <div class="${e.cover_class}"></div>
            <div class="div-block-61">
              <p class="tipoherramienta">${e.functional_category}<br></p>
              <h1 class="tituloherramienta">
                ${e.product_title}
                <br>
              </h1>
              <a href="resultado-herramienta.html?toolID=${e.id}" class="vinculo">Detalle del recurso→</a></div>
          </div>`
        )
      }

    function download(data, filename) {
        var blob, url;
        blob = new Blob([data], {
            type: 'application/octet-stream'
        });
        url = window.URL.createObjectURL(blob);

        var link = document.createElement("a");
        link.download = filename.split('/').pop();
        link.href = url;
        link.setAttribute("target", "_blank");
        link.click();
        link.remove();
  
        setTimeout(function () {
            return window.URL.revokeObjectURL(url);
        }, 1000);

    }
  

    function addToolsToDB(db, tool, data) {
        // Prepara la herramienta, eliminando las herramientas relacionas, el path hacia el archivo y agregando el binario.
        delete tool.RELATED;
        tool.tool_files_stored = data;

        var request = db.transaction(['tools'], 'readwrite')
            .objectStore('tools')
            .put(tool);

        request.onsuccess = function (event) {
            console.log('The data has been written successfully');
            for (let d of data) {
                download(d.data, d.filename);
            }
        };

        request.onerror = function (event) {
            // TODO: Indicar al usuario que no se pudo guardar el archivo.
            console.log('The data has been written failed');
            for (let d of data) {
                download(d.data, d.filename);
            }
        }
    }

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    }

    function ajaxCall(db) {
        $.ajax({
            method: "GET",
            url: `/api/tools/${getUrlParameter('toolID')}`,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8'
        }).done(tool => {
            printTool(tool);
            $('#download-tool').click(function () {
                var files = [];
                var i = 0;
                for (let file of tool.tool_files) {
                    var oReq = new XMLHttpRequest();
                    oReq.open("GET", file.file, true);
                    oReq.responseType = "arraybuffer";
                    oReq.onload = function (oEvent) {
                        var arrayBuffer = oEvent.srcElement.response;
                        if (arrayBuffer == null) {
                            alert('No se pudo descargar el archivo');
                            return;
                        }
                        files.push({
                            data: arrayBuffer,
                            filename: file.file
                        });
                        i++;
                        if (db && i == tool.tool_files.length) {
                            addToolsToDB(db, tool, files);
                        } else if (!db) {
                            download(arrayBuffer, file.file);
                        }
                    };
                    oReq.send();
                }
            });

            if (tool.related.length > 0) {
                var k = 0;
                for (const t of tool.related) {
                    addCard(t, k++ == 0);
                }
            } else {
                $('#related-tools').append(`<p class="text-center">No hay Herramientas relacionadas.</p>`)
            }
        }).catch((XMLHttpRequest, textStatus, errorThrown) => {
            if (XMLHttpRequest.readyState == 0) {
                // Acá no hay red
                $('#tool-content').empty();
                $('#tool-content').append(`<h1 class="my-1">Ops! Parece que no tienes conexión a internet</h1>`)
            } else {
                console.log(errorThrown);
                $('#tool-content').empty();
                $('#tool-content').append(`
              <h1 class="my-4">Algo anda mal con el servidor...
                <small>Si el problema continua, intenta contactar al administrador.</small>
              </h1>
              `);
            }
        });
    }

    openDB().then(db => {
        ajaxCall(db);
    }).catch(error => {
        // TODO: Indicar al usuario
        ajaxCall(null);
    });


    function printTool(e) {
        $('#tool-content').append(`
        <div class="div-block-66">
            <div class="div_cover_resultado">
                <div class="${e.cover_class}"></div>
                <h1 class="nombre_recurso">${e.functional_category}</h1>
            </div>
            <div class="div-block-65">
                <div>
                    <h1 class="descripci-n">Enfoque poblacional:</h1>
                    <h1 class="nombrerecurso">${e.population_approach}<br></h1>
                    <h1 class="descripci-n">Nombre del recurso:</h1>
                    <h1 class="nombrerecurso">${e.product_title}<br></h1>
                    <h1 class="descripci-n">Nombre técnico del recurso:</h1>
                    <h1 class="nombrerecurso">${e.product_name}<br></h1>
                    <h1 class="descripci-n">Descripción:</h1>
                    <h1 class="descripcionrecurso">${e.description}<br></h1>
                </div>
            </div>
        </div>
        <div class="div-block-67"></div>
        <div class="div-block-70">
            <div class="div-block-72">
            <div class="details_block">
                <div class="text-block-7">Autor/es: </div>
                <p class="paragraph-5">${e.author}<br></p>
            </div>
            <div class="details_block">
                <div class="text-block-7">Tema central: </div>
                <p class="paragraph-5">${e.main_topic}<br></p>
            </div>
            <div class="details_block">
                <div class="text-block-7">Localización: </div>
                <p class="paragraph-5">${e.locations}<br></p>
            </div>
            <div class="details_block">
                <div class="text-block-7">Año: </div>
                <p class="paragraph-5">${e.year}<br></p>
            </div>
            <div class="details_block">
                <div class="text-block-7">Tipo de archivo: </div>
                <p class="paragraph-5">${e.files}<br></p>
            </div>
            </div>
            <div>
                <div class="text-block-7">Este producto está en el marco del proyecto: </div>
                <p class="paragraph-5">${e.project_frame}<br></p>
            </div>
        </div>`)
    }
})(jQuery); // End of use strict