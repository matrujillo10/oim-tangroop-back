(function ($) {
    "use strict"; // Start of use strict

    function openDB() {
        return new Promise(function (resolve, reject) {
            var db;
            var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
            var request = indexedDB.open('tools-db');

            request.onerror = function (event) {
                console.log('The database is opened failed');
                reject(event);
            };

            request.onsuccess = function (event) {
                db = request.result;
                resolve(db);
            };

            request.onupgradeneeded = function (event) {
                db = event.target.result;
                var objectStore;
                if (!db.objectStoreNames.contains('tools')) {
                    objectStore = db.createObjectStore('tools', { keyPath: 'id' });
                    objectStore.createIndex('id', 'id', { unique: false });
                }
                resolve(db);
            }
        });
    }

    function download(data, filename) {
        var blob, url;
        blob = new Blob([data], {
            type: 'application/octet-stream'
        });
        url = window.URL.createObjectURL(blob);
  
        var link = document.createElement("a");
        link.download = filename.split('/').pop();
        link.href = url;
        link.setAttribute("target", "_blank");
        link.click();
        link.remove();
  
        setTimeout(function () {
            return window.URL.revokeObjectURL(url);
        }, 1000);
    }  

    function readTool(db, toolID) {
        return new Promise(function (resolve, reject) {
            var transaction = db.transaction(['tools']);
            var objectStore = transaction.objectStore('tools');
            var request = objectStore.get(parseInt(toolID));

            request.onerror = function (event) {
                console.log('Transaction failed');
                reject(event);
            };

            request.onsuccess = function (event) {
                if (request.result) {
                    resolve(request.result);
                } else {
                    reject(event)
                }
            };
        });
    }

    function removeTool(db, toolID) {
        var request = db.transaction(['tools'], 'readwrite')
            .objectStore('tools')
            .delete(toolID);

        request.onerror = function (event) {
            // TODO: Avisar al usuario con un modal
        };

        request.onsuccess = function (event) {
            console.log('The data has been deleted successfully');
            window.location.href = '/descargas.html';
        };
    }

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    }

    function readToolCall(db) {
        if (db) {
            readTool(db, getUrlParameter('toolID')).then(tool => {
                printTool(tool);
                
                $('#download-tool').click(function () {
                    for (let file of tool.tool_files_stored) {
                        download(file.data, file.filename);
                    }
                });

                $('#delete-tool').click(function () {
                    removeTool(db, tool.id);
                });
                
            }).catch(error => {
                console.log(error);
                $('#tool-content').empty();
                $('#tool-content').append(`
                    <h1 class="my-4">Algo anda mal con tu dispositivo...
                        <small>Intenta actualizar tu navegador. Si el problema continua, contacta al administrador.</small>
                    </h1>
                    `);
            });
        } else {
            console.log(error);
            $('#tool-content').empty();
            $('#tool-content').append(`
            <h1 class="my-4">Algo anda mal con tu dispositivo...
                <small>Intenta actualizar tu navegador. Si el problema continua, contacta al administrador.</small>
            </h1>
            `);
        }
    }

    openDB().then(db => {
        readToolCall(db);
    }).catch(error => {
        console.log(error);
        $('#tool-content').empty();
        $('#tool-content').append(`
        <h1 class="my-4">Algo anda mal con tu dispositivo...
            <small>Intenta actualizar tu navegador. Si el problema continua, contacta al administrador.</small>
        </h1>
        `);
    });

    function printTool(e) {
        $('#tool-content').append(`
        <div class="div-block-66">
            <div class="div_cover_resultado">
                <div class="${e.cover_class}"></div>
                <h1 class="nombre_recurso">${e.functional_category}</h1>
            </div>
            <div class="div-block-65">
                <div>
                    <h1 class="descripci-n">Enfoque poblacional:</h1>
                    <h1 class="nombrerecurso">${e.population_approach}<br></h1>
                    <h1 class="descripci-n">Nombre del recurso:</h1>
                    <h1 class="nombrerecurso">${e.product_title}<br></h1>
                    <h1 class="descripci-n">Nombre técnico del recurso:</h1>
                    <h1 class="nombrerecurso">${e.product_name}<br></h1>
                    <h1 class="descripci-n">Descripción:</h1>
                    <h1 class="descripcionrecurso">${e.description}<br></h1>
                </div>
            </div>
        </div>
        <div class="div-block-67"></div>
        <div class="div-block-70">
            <div class="div-block-72">
            <div class="details_block">
                <div class="text-block-7">Autor/es: </div>
                <p class="paragraph-5">${e.author}<br></p>
            </div>
            <div class="details_block">
                <div class="text-block-7">Tema central: </div>
                <p class="paragraph-5">${e.main_topic}<br></p>
            </div>
            <div class="details_block">
                <div class="text-block-7">Localización: </div>
                <p class="paragraph-5">${e.locations}<br></p>
            </div>
            <div class="details_block">
                <div class="text-block-7">Año: </div>
                <p class="paragraph-5">${e.year}<br></p>
            </div>
            <div class="details_block">
                <div class="text-block-7">Tipo de archivo: </div>
                <p class="paragraph-5">${e.files}<br></p>
            </div>
            </div>
            <div>
                <div class="text-block-7">Este producto está en el marco del proyecto: </div>
                <p class="paragraph-5">${e.project_frame}<br></p>
            </div>
        </div>`)
    }
})(jQuery); // End of use strict
