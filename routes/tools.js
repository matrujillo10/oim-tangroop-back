var express = require('express');
var router = express.Router();
var createError = require('http-errors');
var DBManager = require('../DBManager')

var storageHost = 'https://cajaherramientasrpr.com/admin/media/';

function addStorageToFiles(files) {
  for (var i in files) {
    files[i].file = storageHost + files[i].tool_file;
  }
}

/* GET all tools. */
router.get('/', function(req, res, next) {
  var db = new DBManager()
  var result = db.getTools();
  result[0]
  .then((rs) => {
    res.json(rs.rows)
  })
  .catch(e => {
    console.error(e.stack)
    next(createError(500))
  })
  .then(() => result[1].end())
});

/* GET tool details. */
router.get('/:toodID', function(req, res, next) {
  var db = new DBManager()
  var toolID = req.params['toodID'];
  var result = db.getTool(toolID);
  result[0]
  .then((rs) => {
    if (rs.rows.length == 0) {
      next(createError(404))
      return;
    }
    var tool = rs.rows[0];
    var result2 = db.getToolsRelated(toolID);
    result2[0]
    .then((rs2) => {
      tool.related = rs2.rows;
      var result3 = db.getFilesByTool(toolID);
      result3[0]
      .then((rs3) => {
        addStorageToFiles(rs3.rows);
        tool.tool_files = rs3.rows;
        res.json(tool);
      })
      .catch(e => {
        console.error(e.stack)
        next(createError(500))
      })
      .then(() => result3[1].end())
    })
    .catch(e => {
      console.error(e.stack)
      next(createError(500))
    })
    .then(() => result2[1].end())
  })
  .catch(e => {
    console.error(e.stack)
    next(createError(500))
  })
  .then(() => result[1].end())
});

/* GET tools by tags. */
router.post('/by-tags', function(req, res, next) {
  var db = new DBManager()
  var result = db.getToolsByTags(req.body);
  result[0]
  .then((rs) => {
    console.log(rs.rows)
    res.json(rs.rows)
  })
  .catch(e => {
    console.error(e.stack)
    next(createError(500))
  })
  .then(() => result[1].end())
});

/* GET tools related to a tool. */
router.get('/:toodID/related', function(req, res, next) {
  var db = new DBManager()
  var result = db.getToolsRelated(req.params['toodID']);
  result[0]
  .then((rs) => {
    res.json(rs.rows)
  })
  .catch(e => {
    console.error(e.stack)
    next(createError(500))
  })
  .then(() => result[1].end())
});

/* GET all tags of a tool. */
router.get('/:toodID/tags', function(req, res, next) {
  var db = new DBManager()
  var result = db.getTagsByTool(req.params['toodID']);
  result[0]
  .then((rs) => {
    res.json(rs.rows)
  })
  .catch(e => {
    console.error(e.stack)
    next(createError(500))
  })
  .then(() => result[1].end())
});

module.exports = router;
