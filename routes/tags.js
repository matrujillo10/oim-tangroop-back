var express = require('express');
var router = express.Router();
var createError = require('http-errors');
var DBManager = require('../DBManager')

/*
{
  "pattern": '',
  "population_approach": '',
  "functional_category": '',
  "tags": []
}
*/
router.get('/', function(req, res, next) {
  var db = new DBManager()
  if (req.query.pattern && req.query.pattern != '' && req.query.type && (req.query.type == '0' || req.query.type == '1')) {
    var result = db.getTagsByPattern(req.query.pattern, req.query.type);
    result[0]
    .then((rs) => {
      res.json(rs.rows)
    })
    .catch(e => {
      console.error(e.stack)
      next(createError(500))
    })
    .then(() => result[1].end())
  } else {
    next(createError(400))
  }
});

/* GET all tools. */
router.post('/thesaurus', function(req, res, next) {
  var db = new DBManager()
  var result = db.getTags(req.body);
    result[0]
    .then((rs) => {
      res.json(rs.rows)
    })
    .catch(e => {
      console.error(e.stack)
      next(createError(500))
    })
    .then(() => result[1].end())
});

router.post('/pa', function(req, res, next) {
  var db = new DBManager()
  var result = db.getPA(req.body);
    result[0]
    .then((rs) => {
      res.json(rs.rows)
    })
    .catch(e => {
      console.error(e.stack)
      next(createError(500))
    })
    .then(() => result[1].end())
});

router.post('/fc', function(req, res, next) {
  var db = new DBManager()
  var result = db.getFC(req.body);
    result[0]
    .then((rs) => {
      res.json(rs.rows)
    })
    .catch(e => {
      console.error(e.stack)
      next(createError(500))
    })
    .then(() => result[1].end())
});

module.exports = router;